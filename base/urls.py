from django.urls             import path, re_path
from django.conf             import settings
from django.conf.urls.static import static
from django.contrib          import admin

admin.autodiscover()

from . import views

urlpatterns = [
    re_path(r'^$',              views.home,     name='home'        ),
    re_path(r'^signin/$',       views.signin,   name='signin'      ),
    re_path(r'^signup/$',       views.signup,   name='signup'      ),
    re_path(r'^forgot/$',       views.forgot,   name='forgot'      ),
    re_path(r'^signout/$',      views.signout,  name='signout'     ),
    re_path(r'^profile/$',      views.profile,  name='profile'     ),
    re_path(r'^discover/$',     views.discover, name='discover'    ),
    re_path(r'^blog/$',         views.blog,     name='blog'        ),
    re_path(r'^blog_desc/$',    views.blog_desc,name='blog_desc'   ),
    re_path(r'^about/$',        views.about,    name='about'       ),
    re_path(r'^contact/$',      views.contact,  name='contact'     ),
    re_path(r'^add_race/$',     views.add_race, name='add_race'    ),
    re_path(r'^emailpop/$',     views.emailpop, name='emailpop'    ),
    re_path(r'^details/$',      views.details,  name='details'     ),

]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
