from django.shortcuts               import render, redirect
from django.core.mail               import EmailMultiAlternatives
from django.contrib                 import auth
from django.contrib.auth.models     import User
from django.contrib.gis.geoip2      import GeoIP2
#from django.views.generic.edit      import FormView

from .forms                         import LoginForm, SignUpForm
from .models                        import Profile, Event

GeoDB = GeoIP2()

def get_client_ip(request):
    """
    Get the IP for a user requesting access
    :param request: django request object
    :return: str: ip address
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return  '98.109.182.54'


def home(request):
    """ This is the view for the home page
    :param request: django request object
    """
    if request.user.is_authenticated:
        user = request.user
        here = {'longitude': user.profile.longitude, 'latitude': user.profile.latitude}
    else:
        user = None
        try:
            location = GeoDB.coords(get_client_ip(request))
            here = {'longitude':location[0],'latitude':location[1]}
        except Exception as e:
            here = {'longitude':None, 'latitude':None}

    events = Event.objects.featured( here )
    return render( request, 'index.html', {'user': user, 'events':events} )


def signin(request):
    """ Sign in a user
    :param request: django request object
    """

    def submit_form( form ):
        return render(request, 'registration.html', {'form': form})
 
    form = LoginForm(request.POST)
    if not form.is_valid():
        return submit_form(form)

    # Get the name and password and login
    username = form.cleaned_data['username']
    password = form.cleaned_data['password']

    try:
        user = User.objects.get(username = username)
    except User.DoesNotExist:
        try:
            user = User.objects.get(email = username)
        except User.DoesNotExist:
            form.errors['username']  = "User does not exist or wrong password"
            return submit_form(form)

    # Once you have them authenticate
    user = auth.authenticate(username=user.username, password=password)

    # All good bring them back home
    if user is not None and user.is_active:
        auth.login(request, user)
        return redirect('home')
    else:
        form.errors['username']  = "User does not exist or wrong password"
        return submit_form(form)


def signout(request):
    """ Sign out and redirect home
    :param request:
    :return: redirect to home page
    """
    auth.logout(request)
    return redirect('home')


def signup(request):
    """  Sign up form
    :param request:
    :return: redirect home
    """
    def submit_form( form ):
        return render(request, 'registration.html', {'form': form})

    form = SignUpForm(request.POST)
    if not form.is_valid():
        return submit_form( form )

    # Check password input
    username = form.cleaned_data['full_name']
    password = form.cleaned_data['password']

    # Get the email address and double check it to make sure its unique
    email = form.cleaned_data['email']

    if email != u'':
        qry = User.objects.filter(username = username, email=email)
        if qry.count() >= 1:
            form.errors['email'] = "This email is in use. Sign in instead"
            return submit_form(form)

    # Create the user
    try:
        user = User.objects.create_user(username=username, email=email, password=password)
    except:
        form.errors['name'] = 'This name has already been used'
        return submit_form( form )

    user.first_name = username.split()[0]
    user.last_name = username.split()[1]
    user.save()

    # Create profile
    profile = Profile(user=user)
    profile.address = form.cleaned_data['location']
    profile.save()

    # Login the new user
    user = auth.authenticate(username=user.username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)

    return redirect('home')


def forgot( request, username ):
    """ Forgotten password
    :param request: django request object
    :param username: str: user name to send email to
    :return: redirect back home
    """

    try:
        user = User.objects.get(email = username)
    except User.DoesNotExist:
        user = None
        password = None
        
    else:
        password = generate(6)
        user.set_password(password)
        user.save()
    
    # Set up the context
    c = { 'user':user, 'password' :password }

    # Render the message and log it
    template = loader.get_template('letters/forgot.tmpl')
    message = template.render(c)

    subject = 'Race Database Account'
    bcc = [ 'contacts@racedatabase.com' ]
    from_email = '<contacts@racedatabase.com>'
    
    if not user:
        to_email = [ '%s'% ( username ) ]
    else:
        to_email = [ '%s %s <%s>'% ( user.first_name, user.last_name, user.email ) ]
    
    # Send the email
    msg = EmailMultiAlternatives( subject    = subject,
                                  body       = message,
                                  from_email = from_email,
                                  to         = to_email,
                                  bcc        = bcc
                                )

    try:
        msg.send( fail_silently = False )
    except:
        pass
 
    return password


def profile(request):
    """ Edit the users profile
    :param request:
    :return:
    """
    return render(request, 'user-profile.html', {'user': request.user})


def discover(request):
    """ Discover races
    :param request:
    :return:
    """
    return render(request, 'discover.html', {'user': request.user})


def about(request):
    """ About race database
    :param request:
    :return:
    """
    return render(request, 'about.html', {'user': request.user})


def contact( request ):
    """ Contact us
    :param request:
    :return:
    """
    return render(request, 'contact.html', {'user':request.user})


def add_race(request):
    """ Add a race
    :param request:
    :return:
    """
    return render(request, 'add-race.html', {'user':request.user})


def details(request):
    """ Details about a race
    :param request:
    :return:
    """
    return render(request, 'details.html', {'user':request.user})


def blog_desc(request):
    """ An individual blog posting
    :param request:
    :return:
    """
    return render(request, 'blog_desc.html', {'user':request.user})


def blog(request):
    """ Show the blogs
    :param request:
    :return:
    """
    return render(request, 'blog.html', {'user':request.user})


def emailpop(request):
    """ Email contact request form
    :param request:
    :return:
    """
    return render(request, 'blog.html', {'user':request.user})
