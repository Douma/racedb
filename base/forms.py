from django import  forms

class SignUpForm(forms.Form):

    full_name       = forms.RegexField  ( label = 'User Name:',
                                            required = False,
                                            max_length=45,
                                            regex=r'^((\w((\s|\@|\!){0,1})){2,})+$',
                                            widget= forms.TextInput(attrs={'class':'supf','size':512} )
                                        )

  
    password        = forms.CharField   ( max_length = 45,
                                            widget = forms.PasswordInput(attrs={'class':'supf','size':128} )
                                        )
  
  
    email           = forms.EmailField  ( required = False,
                                            max_length = 60,
                                            widget= forms.TextInput(attrs={'class':'supf','size':512})
                                        )


    location        = forms.RegexField  ( required = False,
                                            max_length = 45,
                                            regex=r"^[a-zA-Z0-9,' ']+$",
                                            widget = forms.TextInput(attrs={'class':'supf','size':512})
                                        )


class LoginForm(forms.Form):
    username        = forms.CharField   ( max_length = 45,
                                          required =True
                                        )
                                        
    password        = forms.CharField   ( max_length = 45,
                                          required = True
                                        )


