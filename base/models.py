from django.db                  import models
from django.db                  import connection
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser


from datetime import datetime

class Profile( models.Model ):
    user         = models.OneToOneField( User, 
                                         on_delete=models.CASCADE,
                                         related_name="profile"
                                       )
    address      = models.CharField(max_length=100,  blank = True, null = True)
    longitude    = models.FloatField( null = True, blank = True )
    latitude     = models.FloatField( null = True, blank = True )
    mugshot      = models.ImageField(upload_to = "profiles", null = True, blank = True)
    is_pro       = models.BooleanField(default = False)
    is_organizer = models.BooleanField(default = False)
    web_site     = models.URLField(null = True, blank = True)

    def __unicode__(self):
        if self.user.username == u'':
            return str(self.pk)
        return self.user.username+self.user.email


class EventManager(models.Manager):

    def for_user(self, userid):
        pass


    def nearby(self, location ):
        pass


    def featured(self, location ):
        today = datetime.today()
        if not location['longitude']:
            location['longitude'] = -74
        if not location['latitude']:
            location['latitude'] = 40
        data = self.raw(""" SELECT *
	                        FROM public.base_event
	                        WHERE longitude between ({} -1) and ({} +1)
	                        and  latitude between ({} -0.5) and ({} + 0.5);
                        """.format( location['longitude'],location['longitude'],
                                    location['latitude'],location['latitude'] )
                        )
        return [d for d in data]
    


class Event(models.Model):
    name         = models.CharField(max_length=256,  blank = False, null = False)
    date         = models.DateTimeField(blank = False, null= False)
    location     = models.CharField(max_length=256,  blank = True, null = True)
    longitude    = models.FloatField( null = True, blank = True )
    latitude     = models.FloatField( null = True, blank = True )
    registration = models.URLField(null = True, blank = True, max_length=1024)
    kind         = models.CharField(max_length=256, blank = True, null = True )
    price        = models.DecimalField(max_digits=6, decimal_places=2, blank = True, null = True)
    contact      = models.ForeignKey( User, on_delete=models.CASCADE, null = True )
    rating       = models.IntegerField(default = 3)
    description  = models.TextField(blank=True, null=True)
    featured     = models.IntegerField( default = 0 )
    objects      = EventManager()
    

    def __unicode__(self):
        return self.name + '-'+self.date