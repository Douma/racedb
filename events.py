import os
import gspread
import django
import arrow

import geo 

from datetime                       import datetime
from oauth2client.service_account   import ServiceAccountCredentials
from time import strptime

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "racedb.settings")
#from django.conf                    import settings
#settings.configure(default_settings=racedb.settings, DEBUG=True)
django.setup()


CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))
credentials = ServiceAccountCredentials.from_json_keyfile_name( CURRENT_DIR + '/racedb.json', 
                                                                 scopes = ['https://spreadsheets.google.com/feeds',
                                                                           'https://www.googleapis.com/auth/drive' ]
                                                              )
from base.models import Event

def main():

    drive  = gspread.authorize(credentials)
    rdb    = drive.open('Race Database')
    sheets = rdb.worksheets()
    
    for sheet in sheets:
        for r, row in enumerate( sheet.get_all_values() ):
            if r == 0:
                continue
            day = row[0].split('-')[0]
            try:
                day = arrow.get( day, "M/D/YY")
            except Exception as e:
                print ( row, str(e) )
                continue
            
            location = geo.geocode( row[2] )
            try:
                event,created = Event.objects.get_or_create( date = day.datetime,
                                                             name = row[1],
                                                             location = row[2]
                                                            )
            except Exception as e:
                print ( "Error {} for {}".format( str(e), row ))
                continue
            
            if not event.longitude and not event.latitude and location:
                event.latitude  = location.get('lat',None)
                event.longitude = location.get('lng',None)
            
            event.registration  = row[3]
 
            try:
                event.save()
            except Exception as e:
                print( e )
    pass

if __name__ =="__main__":
    main()
